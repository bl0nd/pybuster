# Pybuster Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## 0.1.3 (2018-06-08)
### Features


### Modifications


## 0.1.2 (2018-06-07)
### Features
 - Add cookie specification ([02f0443](https://github.com/bl0nd/pybuster/commit/02f0443))
 - Add timeout period specification ([02f0443](https://github.com/bl0nd/pybuster/commit/02f0443))
 - Add color to banner and error messages ([676a99f](https://github.com/bl0nd/pybuster/commit/676a99f))
 - Add KeyboardInterrupt handling. ([4ae1bcd](https://github.com/bl0nd/pybuster/commit/4ae1bcd))
 - Add colored status codes. ([22339ac](https://github.com/bl0nd/pybuster/commit/22339ac))

### Modifications
 - Error messages are now in Context, Problem, Solution format ([676a99f](https://github.com/bl0nd/pybuster/commit/676a99f))
 - Replaced all .format() with fstrings ([02f0443](https://github.com/bl0nd/pybuster/commit/02f0443))
 - Changed Banner look ([02f0443](https://github.com/bl0nd/pybuster/commit/02f0443))
 - Updated documentation ([02f0443](https://github.com/bl0nd/pybuster/commit/02f0443))
 - Changed the name of *brute_forcer.py* to *buster.py* ([402b95](https://github.com/bl0nd/pybuster/commit/402b95))
 - Replaced output config one-liners with something more readable ([676a99f](https://github.com/bl0nd/pybuster/commit/676a99f))
 - Moved everything in *manager.py* into *buster.py* ([676a99f](https://github.com/bl0nd/pybuster/commit/676a99f))
 - **Removed**: *manager.py* ([402b95](https://github.com/bl0nd/pybuster/commit/402b95))
 - Spaced out status code and length in results ([22339ac](https://github.com/bl0nd/pybuster/commit/22339ac))


## 0.1.1 (2018-06-05)
### Features
 - Add recursive brute forcing ([05a7df8](https://github.com/bl0nd/pybuster/commit/05a7df8))

### Modifications
 - The thread pool is now opened in a 'with' statement ([05a7df8](https://github.com/bl0nd/pybuster/commit/05a7df8))


## 0.1.0 (2018/05/29)
 - Initial release
