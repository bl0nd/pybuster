# Pybuster

Pybuster is a Python 3 port of Dirbuster, a directory and file brute forcer. It was made because I was getting tired of repeatedly installing Go on my boxes just for gobuster. Most boxes come with Python preinstalled now so that makes things a lot easier.

## Features
* Multithreading
* Recursive brute forcing
* User-Agent specification
* Output configuration

## Usage
    $ python3 pybuster.py [mode] [option(s)] wordlist url

        Python implementation of DirBuster.

        required arguments:
        -w WORDLIST                     path to wordlist
        -u URL                          URL or IP

        optional arguments:
        -t THREADS                      number of threads [Default: 4]
        -x EXTENSION1[,EXTENSION2...]   set file extension(s) to look for
        -a USER-AGENT                   set the User-Agent [Default: PyBuster (v0.1.0)]
        -l                              include response body length in results [Default: Off]
        -e                              display full URL in results [Default: Off]
        -r                              perform recursive brute forcing [Default: Off]
        -h, --help                      show this help message and exit
