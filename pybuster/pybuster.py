#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    pybuster.py
    ~~~~~~~~~~~
    
    Tool for brute forcing directories and files.

TODO:
    -- Functionality:
        --- HTTPS support
        --- Basic Auth (-P, -U)
    -- Find a better place for timing.
    -- Figure out the best way to fallback to GETs if HEADs fail.
        a certain amount of times.
    -- Maybe find a way to order our requests.
    -- Better IO error solutions.

FIXME:
    -- The program hangs instead of exiting when a sys.exit()
        happens with threaded busting.

NOTES:
    -- There's probably a better way to parse URLs/IPs.
    -- Find a way to get rid of DEFAULT_PORTS?
"""

import os
import sys
import time
import argparse
from colored import fg, attr

from buster import Buster

GREEN  = fg(10)
YELLOW = fg(11)
RESET  = attr(0)
PLUS   = f'[{GREEN}+{RESET}]'


"""
[+++++++++++++++++++++++++++++++++++++++++++++]
                   Parser
[+++++++++++++++++++++++++++++++++++++++++++++]
"""
class MyParser(argparse.ArgumentParser):
    """Overriding class for custom help/usage message."""
    @staticmethod
    def print_help():
        """Print custom help menu."""
        print('''\
Usage: python3 pybuster.py [mode] [option(s)] wordlist url

Python implementation of DirBuster.

required arguments:
  -w WORDLIST                     path to wordlist
  -u URL                          URL or IP

optional arguments:
  -t THREADS                      number of threads [Default: 4]
  -x EXTENSION1[,EXTENSION2...]   set file extension(s) to look for
  -a USER-AGENT                   set the User-Agent [Default: PyBuster (v0.1.0)]
  -c COOKIE1[,COOKIE2...]         set any cookies you may need [Default: None]
  -l                              include response body length in results [Default: Off]
  -e                              include full URL in results [Default: Off]
  -r                              perform recursive busting [Default: Off]
  -T TIME                         set the request timeout period [Default: 3]
  -h, --help                      show this help message and exit
''')


def create_parser():
    """Create a command-line parser.

    Returns:
        The command-line flags and their state as a Namespace object.
    """
    parser = MyParser(description="Python implementation of the Dirbuster.")
    required = parser.add_argument_group(title='required arguments')

    if len(sys.argv) == 1:
        sys.exit(parser.print_help())

    required.add_argument('-w', required=True, action='store', dest='word_list')
    required.add_argument('-u', required=True, action='store', dest='url')
    parser.add_argument('-t', type=int, action='store', dest='threads')
    parser.add_argument('-x', action='store', dest='ext')
    parser.add_argument('-a', action='store', dest='agent')
    parser.add_argument('-c', action='store', dest='cookies')
    parser.add_argument('-l', action='store_true', dest='length')
    parser.add_argument('-e', action='store_true', dest='expand')
    parser.add_argument('-r', action='store_true', dest='recursive')
    parser.add_argument('-T', action='store_true', dest='timeout')

    return parser.parse_args()


"""
[++++++++++++++++++++++++++++++++++++++++++++]
                   Banner
[++++++++++++++++++++++++++++++++++++++++++++]
"""
def print_banner(buster, args):
    """Print Pybuster banner.
    
    Args:
        buster: A Buster instance.
        args: A Namespace object containing the command-line flags
            and their state.
    """
    border = '-' * (len(buster.word_list) + 60)
    spacing = len(buster.word_list) + 41

    print(f'.{border}-.')
    print(f'| {GREEN}Pybuster v0.2.0{RESET}'                  + f'{"|".rjust(74)}')
    print(f'|'                                                + f'{"|".rjust(spacing + 21)}')
    print(f'| {PLUS} Mode          : Directory'               + f'{"|".rjust(spacing - 9)}')
    print(f'| {PLUS} URL/Domain    : {buster.url}'            + f'{"|".rjust(spacing - len(buster.url))}')
    print(f'| {PLUS} Wordlist      : {buster.word_list}'      + f'{"|".rjust(spacing - len(buster.word_list))}')
    print(f'| {PLUS} Word Count    : {buster.resource_count}' + f'{"|".rjust(spacing - len(str(buster.resource_count)))}')
    print(f'| {PLUS} Threads       : {buster.threads}'        + f'{"|".rjust(spacing - len(str(buster.threads)))}')
    print(f'| {PLUS} Timeout       : {buster.timeout}'        + f'{"|".rjust(spacing - len(str(buster.timeout)))}')
    
    if args.agent:
        print(f'| {PLUS} User-Agent    : {buster.user_agent}'   + f'{"|".rjust(spacing - len(buster.user_agent))}')
    if args.cookies:
        print(f'| {PLUS} Cookies       : {buster.cookies}'      + f'{"|".rjust(spacing - len(buster.cookies))}')
    if args.length:
        print(f'| {PLUS} Body Length   : {buster.force_length}' + f'{"|".rjust(spacing - 4)}')
    if args.expand:
        print(f'| {PLUS} Expanded Mode : {buster.force_expand}' + f'{"|".rjust(spacing - 4)}')
    if args.recursive:
        print(f'| {PLUS} Recursive     : {buster.recursive}'    + f'{"|".rjust(spacing - 4)}')
    print(f"'{border}-'")


"""
[+++++++++++++++++++++++++++++++++++++++++++++]
                   Main
[+++++++++++++++++++++++++++++++++++++++++++++]
"""
def main():
    """Main program, used when run as a script.
    
    Create a buster instance to handle config options,
        creates a parser, parses arguments given, sets up
        our HTTP client, prints banner, starts busting.

    Returns:
        An exit code to be passed to sys.exit(). (None indicates success)
    """
    buster = Buster()
    args = create_parser()

    buster.setup_buster(args)
    print_banner(buster, args)
    buster.run()

    return None


if __name__ == "__main__":
    try:
        start = time.time()
        main()
        end = time.time()
        print(f'\n\nTime: {end-start} seconds')
    except KeyboardInterrupt as e:
        print(f'\n\n{YELLOW}ERROR{RESET}: {e.__class__.__name__}\n')
