"""
    buster.py
    ~~~~~~~~~

    This module handles creating and setting up the HTTP client,
        sending requests to the target URL, and printing the
        results of the busting.
"""
import os
import sys
import validators

import requests.exceptions
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter

from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool

from functools import partial
from urllib.parse import urlparse
from colored import fg, attr


# Colored Error Text
ERROR    = f'{fg(11)}ERROR{attr(0)}'
CONTEXT  = f'{fg(208)}Context{attr(0)}'
PROBLEM  = f'{fg(9)}Problem{attr(0)}'
SOLUTION = f'{fg(10)}Solution{attr(0)}'

# Colored Status Code
SUCCESS  = '{}{}{}'.format(fg(10), '{}',attr(0))
REDIRECT = '{}{}{}'.format(fg(11), '{}', attr(0))
FAILURE  = '{}{}{}'.format(fg(9), '{}', attr(0))

DEFAULT_PORTS = {
  "acap": 674, 
  "afp": 548, 
  "dict": 2628, 
  "dns": 53, 
  "file": None, 
  "ftp": 21, 
  "git": 9418, 
  "gopher": 70, 
  "http": 80, 
  "https": 443, 
  "imap": 143, 
  "ipp": 631, 
  "ipps": 631, 
  "irc": 194, 
  "ircs": 6697, 
  "ldap": 389, 
  "ldaps": 636, 
  "mms": 1755, 
  "msrp": 2855, 
  "msrps": None, 
  "mtqp": 1038, 
  "nfs": 111, 
  "nntp": 119, 
  "nntps": 563, 
  "pop": 110, 
  "prospero": 1525, 
  "redis": 6379, 
  "rsync": 873, 
  "rtsp": 554, 
  "rtsps": 322, 
  "rtspu": 5005, 
  "sftp": 22, 
  "smb": 445, 
  "snmp": 161, 
  "ssh": 22, 
  "steam": None, 
  "svn": 3690, 
  "telnet": 23, 
  "ventrilo": 3784, 
  "vnc": 5900, 
  "wais": 210, 
  "ws": 80, 
  "wss": 443, 
  "xmpp": None
}


class Buster(object):
    """Class for brute forcing directories and files.

    Attributes:
        protocol:       (String) The web protocol to use.
        url:            (String) The target URL to bust.
        port:           (int) The web protocol's port.
        word_list:      (String) The word list to use.
        extension:      (set) The file extension(s) to look for.
        threads:        (int) Number of threads to use.
        http_session:   (Session) A Session instance from which all requests
                          will be made.
        user_agent:     (String) The User-Agent to use [default: Pybuster (v0.2.0)].
        cookies:        (String) Any cookies to send.
        timeout:        (int) The timeout period for requests [default: 3].
        resource_count: (int) The number of resources in the word list used.
        request_method: (String) The request method to use (HEAD or GET).
        include_files:  (boolean) If True, Pybuster will look for files with the
                          extension(s) specified in `extension`.
        recursive:      (boolean): If True, Pybuster will bust recursively.
        force_length:   (boolean): If True, Pybuster will include the response
                          body's length in the results.
        force_expand:   (boolean): If True, Pybuster will include the full URL
                          of found resources in the results.
    """
    def __init__(self):
        """Constructor. See class docstring"""
        # Target specification
        self.protocol          = ''
        self.url               = ''
        self.port              = 0

        # Items to look for
        self.word_list         = ''
        self.extension         = set()

        # Threads
        self.threads           = 4

        # HTTP stuff
        self.http_session      = None
        self.user_agent        = 'PyBuster (v0.1.0)'
        self.cookies           = None
        self.timeout           = 3

        # HTTP request stuff
        self.resource_count    = 0
        self.request_method    = ''

        # Brute forcing config
        self.include_files     = False  # Get rid of this, we only need `extension`
        self.recursive         = False

        # Output config
        self.force_length      = False
        self.force_expand      = False
        self.start_point       = ''

        # For some reason, this redirects timeouts to ConnectionErrors
        # HTTP ConnectionError retry stuff
        # We'll retry 3 times, adding delays between attempts.
        # self.retry = Retry(connect=3, backoff_factor=0.5)
        # self.adapter = HTTPAdapter(max_retries=self.retry)
        # self.http_session.mount('http://', self.adapter)
        # self.http_session.mount('https://', self.adapter)


    def setup_buster(self, args):
        self.parse_arguments(args)
        self.setup_http_client()
        self.count_resources()


    def parse_arguments(self, args):
        """Set buster's attributes based on parser args.
        
        Args:
            buster: A Buster instance.
            args: A Namespace object containing the command-line flags
                and their state.
        """
        # Required args
        if args.url:
            if validators.ipv4(args.url):
                self.protocol = 'http'  # Fix this
                self.url      = f'{self.protocol}://{args.url}'
                self.port     = 80
            elif validators.url(args.url):
                self.protocol = urlparse(args.url).scheme
                self.url      = args.url
                self.port     = DEFAULT_PORTS.get(self.protocol)
            else:
                sys.exit(
                    f"\n{ERROR}:    Invalid/incomplete URL\n\n"
                    + f"{CONTEXT}:  Instantiating target URL, protocol, and port.\n"
                    + f"{PROBLEM}:  URL is invalid/incomplete.\n"
                    + f"{SOLUTION}: For hostname URLs, make sure they're of the format: hostname://URL.\n"
                    + f"\t  For IP addresses, you don't need to have a protocol specified.\n")
        if args.word_list:
            if os.path.isfile(args.word_list):
                self.word_list = args.word_list
            else:
                sys.exit(
                    f"\n{ERROR}:    Nonexistent file.\n\n"
                    + f"{CONTEXT}:  Instantiating word list file.\n"
                    + f"{PROBLEM}:  The specified file does not exist..\n"
                    + f"{SOLUTION}: Make sure the file name is accurate.\n")

        # Optional args
        if args.threads:
            self.threads = args.threads
        if args.ext:
            self.include_files = True
            self.extension = set(['.' + ext for ext in args.ext.split(',')])
        if args.agent:
            self.user_agent = args.agent
        if args.cookies:
            self.cookies = args.cookies
        if args.length:
            self.force_length = args.length
        if args.expand:
            self.force_expand = args.expand
        if args.recursive:
            self.recursive = args.recursive
        if args.timeout:
            self.timeout = int(args.timeout)


    def setup_http_client(self):
        """Set up the HTTP client and its headers."""
        self.http_session = requests.Session()

        # Update headers
        self.http_session.headers.update({'User-Agent': self.user_agent})
        if self.cookies:
            self.http_session.headers.update({'Cookies': self.cookies})


    def count_resources(self):
        """Calculate and set how many requests there are to make.

        Raises:
            IOError: An Exception occurred accessing the word list file.
                         Most likely, it's because `self.word_list` isn't
                         set yet for some reason.
        """
        total_resources = 0
        try:
            with open(self.word_list, 'r') as f:
                for line in f:
                    if not line.startswith('#') and line != '\n':
                        total_resources += 1
        except IOError as e:
            # Probably need better solutions
            # Also, not sure if permissions is an IOError, have to test that
            sys.exit(
                f"\n{ERROR}:    {e.__class__.__name__}\n\n"
                + f"{CONTEXT}:  Accessing the word list file.\n"
                + f"{PROBLEM}:  Access was unsuccessful/refused.\n"
                + f"{SOLUTION}: Make sure you have sufficient file permissions.\n"
                + f"\t  Make sure there's not a  connection problem (e.g., bad cable).")        
        self.resource_count = total_resources


    def configure_output(self, r_length, full_url, resource, start_point):
        """Set any output configuration settings.
        
        Configure the expaned URL and appropriate response body length
            value for the printed results if their respective flags are
            set.

        Then, calculate the appropriate spacing and creates an outline
            of the result line that's going to be printed.

        Args:
            r_length: (int) The response body's length.
            full_url: (String) The absolute URL.
                For example:
                
                    http://10.10.10.10/resource

            resource: (String) The resource found while brute forcing.
                For example:

                    /resource

            start_point: (String) The starting directory from which to
                start busting (used for recursive busting).
        
        Returns:
            A String outlining the result line to be printed
                For example:
                
                    10.10.10.10/hi        (Status: 200) (Length: 0)

                The String is formatted later in the actual print() call
                later with the corresponding color based on the response's
                status code.

                For readability and easier debugging, the String variable itself
                is broken into 3 sections, each indicated by a separate fstring:

                    Resource Name  |  Status Code  |  Response Body Length
        """
        if self.force_expand:
            self.resource = full_url
            self.start_point = '' if start_point and self.force_expand else start_point
        else:
            self.resource = resource

        if self.force_length:
            self.length = f'(Length: {r_length if r_length else 0})'
            spacing = 72 - (len(self.resource) + len(self.start_point))
        else:
            self.length = ''
            spacing = 87 - (len(self.resource) + len(self.start_point))

        result  = f'{self.start_point}{self.resource}' + f'{"(Status: ".rjust(spacing)}' + '{}) ' + f'{self.length}'
        return result


    def run(self):
        """Send threaded requests.
        
        First, check for HEAD support on the server since it's
            faster and less costly than GET.
        Then create a pool of `self.threads` threads to carry
            out the threaded brute forcing.
        """
        # Set our request method
        if self.head_support():
            self.request_method = 'HEAD'
        else:
            self.request_method = 'GET'

        # Multi-thread brute forcing
        with open(self.word_list) as f:
            # for line in f:
            #     self.send_request(line)
            with ThreadPool(processes=self.threads) as pool:
                pool.map(self.send_request, f)


    def head_support(self):
        """Check if the server supports HEAD requests.
        
        HEAD is set as the request method if:
            OPTIONS isn't supported.
            OPTIONS is supported but there's no Allow header.
            HEAD is a value in the Allow header returned by the OPTIONS method.

        GET is set as the request method if:
            HEAD is not in the Allow header returned by the OPTIONS method.

        Returns:
            A boolean indicating whether to use the HEAD method (True) or
                GET method (False).
        
        Raises:
            ConnectTimeout: The connection timed out after `self.timout` seconds.
            ConnectionError: The connection was refused due to exceeding max retries.
        """
        # See if OPTIONS is supported
        try:
            r = self.http_session.options(self.url, timeout=self.timeout)
        except requests.exceptions.Timeout as e:
            sys.exit(
                f"\n{ERROR}:    {e.__class__.__name__}\n\n"
                + f"{CONTEXT}:  Sending OPTIONS request to see if HEAD is supported.\n"
                + f"{PROBLEM}:  The connection timed out after {self.timeout} seconds.\n"
                + f"{SOLUTION}: Set a longer timeout period with the -T flag.\n"
                + f"\t  If that doesn't work, make sure the server's running and try again.\n")
        except requests.exceptions.ConnectionError as e:
            sys.exit(
                f"\n{ERROR}:    {e.__class__.__name__}\n\n"
                + f"{CONTEXT}:  Sending OPTIONS request to see if HEAD is supported.\n"
                + f"{PROBLEM}:  The connection was refused due to max retries exceeded.\n"
                + f"{SOLUTION}: Make sure the server is running.\n"
                + f"\t  Wait for a couple of seconds before retrying again.")
        
        # Default to HEAD if we can't find out if it's supported through OPTIONS
        if r.status_code >= 400 or 'Allow' not in list(r.headers.keys()):
            return True

        if 'HEAD' in r.headers['Allow']:
            return True
        else:
            return False


    def send_request(self, resource, start_point=''):
        """Send HTTP requests.
        
        If a resource is valid:
            1. Attempt to find directories matching the resource name.
                   If found, print the results.
            2. Check to see if recursive busting is set. If it is,
                   recall itself when a directory is found.
            3. Attempt to find files matching the resource name.
                   If found, print the results.

        Args:
            resource: (String) The resource found while brute forcing
                (e.g., resource).
            start_point: (String) The starting directory from which to
                start busting (used for recursive busting).
            
        Returns:
            None if the resource obtained from the word list is either a
                comment (starting with #) or a blank line.
        
        Raises:
            ConnectTimeout: The connection timed out after `self.timout` seconds.
            ConnectionError: The connection was refused due to exceeding max retries.
        """
        # Ignore comments or blank lines
        if resource.startswith('#') or resource == '\n':
            return None

        # Request resource
        resource = '/' + resource.strip()
        try:
            # Directories
            full_dir_url = self.url + start_point + resource + '/'
            response = self.http_session.request(f'{self.request_method}', full_dir_url, timeout=self.timeout)

            if response.status_code != 404:
                result = self.configure_output(
                    response.headers.get('Content-Length'),
                    full_dir_url,
                    resource,
                    start_point) 

                if response.status_code < 299:
                    print(result.format(SUCCESS.format(response.status_code)))
                elif response.status_code < 399:
                    print(result.format(REDIRECT.format(response.status_code)))
                elif response.status_code < 499:
                    print(result.format(FAILURE.format(response.status_code)))

                # Recursive
                if self.recursive:
                    with open(self.word_list) as f:
                        send_request = partial(self.send_request, start_point=f'{start_point}{resource}')
                        list(map(send_request, f))
                    # with open(self.word_list) as f:
                    #     for line in f:
                    #         self.send_request(line, start_point=start_point+resource)

            # Files
            if self.include_files:
                for ext in self.extension:
                    full_file_url = self.url + start_point + resource + ext
                    response = self.http_session.request(f'{self.request_method}', full_file_url, timeout=self.timeout)

                    if response.status_code != 404:
                        result = self.configure_output(
                            response.headers.get('Content-Length'),
                            full_file_url,
                            resource + ext,
                            start_point)

                        if response.status_code < 299:
                            print(result.format(SUCCESS.format(response.status_code)))
                        elif response.status_code < 399:
                            print(result.format(REDIRECT.format(response.status_code)))
                        elif response.status_code < 499:
                            print(result.format(FAILURE.format(response.status_code)))
        except requests.exceptions.Timeout as e:
            sys.exit(
                f"\n{ERROR}:    {e.__class__.__name__}\n\n"
                + f"{CONTEXT}:  Sending {self.request_method} request to see if {resource} exists at {self.url}.\n"
                + f"{PROBLEM}:  The connection timed out after {self.timeout} seconds.\n"
                + f"{SOLUTION}: Set a longer timeout period with the -T flag.\n"
                + f"\t  If that doesn't work, make sure the server's running and try again.\n")
        except requests.exceptions.ConnectionError as e:
            sys.exit(
                f"\n{ERROR}:    {e.__class__.__name__}\n\n"
                + f"{CONTEXT}:  Sending {self.request_method} request to see if {resource} exists at {self.url}.\n"
                + f"{PROBLEM}:  The connection was refused due to max retries exceeded.\n"
                + f"{SOLUTION}: Make sure the server is running.\n"
                + f"\t  Wait for a couple of seconds before retrying again.")  # Deal with SSLError https://www.google.com/publications